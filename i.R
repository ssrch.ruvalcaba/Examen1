require(matrixStats)
require(ggplot2)
setwd('C:/Users/ssrch/Desktop/Iteso/Modelos_De_Credito_Sar/Examen1/')
data_test <- read.csv('test_ultima.csv', head=T)

#Realizamos el método de bootstrapping como lo vimos en clase
cum_utilities <- function(data_model){
  
  hist_all<-hist(data_model[,'nn'],plot=F)
  breaks<-hist_all$breaks
  hist_good<-hist(data_model[data_model$is_good==1,'nn'],col=rgb(0, 0, 1, 0.5),breaks=breaks,main = "Histograma Red neuronal")
  hist_bad<-hist(data_model[data_model$is_good==0,'nn'],col=rgb(1, 0, 0, 0.5),add=T,breaks=breaks)
  
  accepted_bin<-(hist_good$counts+hist_bad$counts)/sum(hist_good$counts+hist_bad$counts)
  default_bin<-hist_bad$counts/(hist_good$counts+hist_bad$counts)
  default_bin[is.nan(default_bin)]<-0
  
  revenue_good <- 10000 * accepted_bin * (1 - default_bin) * array_amount_good * array_total_interest_good
  revenue_bad<- 10000 * accepted_bin * (default_bin) * array_amount_bad * array_total_interest_bad
  default_cost <- 10000 * accepted_bin * default_bin * array_amount_bad * array_default
  marketing    <- 1000000
  utilities    <- revenue_good + revenue_bad - default_cost 
  cumulative_utilities <-rev(cumsum(rev(utilities)))-marketing #Revierto el orden del arreglo
  
  return(cumulative_utilities)
}

bootstrap_df <-c()
for (bt in 1:30){
  
  bootstrap_rows <- sample(row.names(data_test),
                           nrow(data_test), 
                           replace = T ) 
  
  bootstrap_sample <- data_test[bootstrap_rows,]
  
  bootstrap_util <- cum_utilities(bootstrap_sample)
  bootstrap_df<-rbind(bootstrap_df,as.numeric(bootstrap_util))
}
# Lo graficamos con una desviación estándar a cada lado para cubrir la probabilidad
# de 68%, sin embargo, al final convergen de todas maneras y la variación no es tan notoria
mean<- colMeans(bootstrap_df)
minor <- colMeans(bootstrap_df) - colSds(bootstrap_df) #menos 1 desv
mayor <- colMeans(bootstrap_df) + colSds(bootstrap_df) #mas 1 desv

some.data <- data.frame(countdown = mean, minor + mayor)

some.plot0 <- ggplot() +
  geom_line(data = some.data, aes(x = score_cuts, y = countdown),color="black")

some.plot1<- some.plot0 + geom_line(data = some.data, aes(x = score_cuts, y = minor),color="blue")+
  geom_line(data = some.data, aes(x = score_cuts, y = mayor),color="blue") + ggtitle("Utilidades acumuladas") + xlab ("breaks") + ylab ("Pesos")
some.plot1
